package io.golnar.plato;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import io.golnar.plato.models.ClientActivity;
import io.golnar.plato.ui.games.GamesFragment;
import io.golnar.plato.ui.home.HomeFragment;
import io.golnar.plato.ui.people.PeopleFragment;

public class HomeActivity extends AppCompatActivity {
    private static final String TAG = "HomeActivity";

    BottomNavigationView bottomNavigation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Log.i(TAG, "onCreate: home page is created.");

//        startActivity(new Intent(this, ClientActivity.class));

        bottomNavigation = findViewById(R.id.bottom_navigation);

        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.bottom_navigation_home:
                        HomeFragment homeFragment = new HomeFragment();
                        openFragment(homeFragment);
                        break;
                    case R.id.bottom_navigation_games:
                        GamesFragment gamesFragment = new GamesFragment();
                        openFragment(gamesFragment);
                        break;
                    case R.id.bottom_navigation_people:
                        PeopleFragment peopleFragment = new PeopleFragment();
                        openFragment(peopleFragment);
                        break;
                }
                return true;
            }
        });
    }

    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container_home_activity, fragment);
//        transaction.addToBackStack(null);
        transaction.commit();
    }
}
