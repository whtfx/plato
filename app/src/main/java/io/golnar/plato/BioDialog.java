package io.golnar.plato;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import io.golnar.plato.R;

public class BioDialog extends AppCompatDialogFragment {
    private String currentBio;
    private EditText editTextBio;
    private BioDialogListener listener;

    public BioDialog(String currentBio) {
        this.currentBio = currentBio;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_custom_dialog, null);

        builder.setView(view)
                .setTitle("Change your Bio")
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String bio = editTextBio.getText().toString();
                listener.applyTextBio(bio);
            }
        });

        editTextBio = view.findViewById(R.id.input_edit_text_dialog);

        if (currentBio.equals("Say something here...") || currentBio.equals("")) {
            editTextBio.setHint("Say something here...");
        } else {
            editTextBio.setText(currentBio);
        }

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (BioDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement BioDialogListener");
        }
    }

    public interface BioDialogListener {
        void applyTextBio(String bio);
    }
}

