package io.golnar.plato;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import io.golnar.plato.ui.games.xox.PrepareGameActivity;

public class ProfileActivity extends AppCompatActivity implements BioDialog.BioDialogListener, UsernameDialog.UsernameDialogListener {
    private static final String TAG = "ProfileActivity";

    ImageView profileImage, xoxGameIcon;
    Button backButton, settingButton;
    TextView usernameProfileActivity, editUsernameProfile, bioText;
    Uri imageUri;

    final int PICK_IMAGE = 100;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Log.i(TAG, "onCreate: profile page is created.");

        backButton = findViewById(R.id.back_button_profile_activity);
        settingButton = findViewById(R.id.setting_profile_activity);
        profileImage = findViewById(R.id.image_profile_activity);
        bioText = findViewById(R.id.bio_text_profile);
        usernameProfileActivity = findViewById(R.id.username_profile);
        editUsernameProfile = findViewById(R.id.edit_username_profile);
        xoxGameIcon = findViewById(R.id.game_item_image_profile);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: back button is clicked");
                onBackPressed();
            }
        });

        settingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: setting button is clicked.");
                Intent settingIntent = new Intent(ProfileActivity.this, SettingActivity.class);
                startActivity(settingIntent);
            }
        });

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: open gallery");
                openGallery();
            }
        });

        bioText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: BioDialog is created.");
                openBioDialog();
            }
        });
        editUsernameProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: UsernameDialog is created.");
                openUsernameDialog();
            }
        });

        xoxGameIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent xoxIntent = new Intent(getApplicationContext(), PrepareGameActivity.class);
                startActivity(xoxIntent);
            }
        });

    }

    public void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imageUri = data.getData();
            profileImage.setImageURI(imageUri);
        }
    }

    public void openBioDialog() {
        BioDialog bioDialog = new BioDialog(bioText.getText().toString());
        bioDialog.show(getSupportFragmentManager(), "bio dialog");

    }

    @Override
    public void applyTextBio(String bio) {
        if (!bio.equals("")) {
            bioText.setText(bio);
        } else {
            bioText.setText("Say something here...");
        }
    }

    public void openUsernameDialog() {
        UsernameDialog usernameDialog = new UsernameDialog(usernameProfileActivity.getText().toString());
        usernameDialog.show(getSupportFragmentManager(), "username dialog");
    }

    @Override
    public void applyTextUsername(String username) {
        /*
            server checks username
         */
        if (!username.equals("")) {
            usernameProfileActivity.setText(username);
        }
    }
}
