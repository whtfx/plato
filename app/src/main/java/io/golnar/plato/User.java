package io.golnar.plato;

import java.io.Serializable;
import java.util.ArrayList;


public class User implements Serializable {
    String username ;
    String password ;
    byte[] profileImage;
    ArrayList<User> friends = new ArrayList<>();
//    ArrayList<Group> groups = new ArrayList<>();

    User(String username , String password , byte[] profileImage) {
        this.username = username;
        this.password = password;
        this.profileImage =  profileImage ;
    }

    public void addFriend(User friend){
        if(!friends.contains(friend)){
            friends.add(friend);
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public byte[] getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(byte[] profileImage) {
        this.profileImage = profileImage;
    }

    public ArrayList<User> getFriends() {
        return friends;
    }

    public void setFriends(ArrayList<User> friends) {
        this.friends = friends;
    }
}
