package io.golnar.plato.ui.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;

import io.golnar.plato.ProfileActivity;
import io.golnar.plato.R;
import io.golnar.plato.models.Chatroom;
import io.golnar.plato.models.Chatroom.Message;
import io.golnar.plato.ui.ChatroomActivity;

public class HomeFragment extends Fragment {
    ImageView profile;
    ArrayList<Chatroom> chatroomList;
    private RecyclerView recycler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        profile = root.findViewById(R.id.profile_toolbar_home);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent profileIntent = new Intent(getActivity(), ProfileActivity.class);
                startActivity(profileIntent);
            }
        });
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        chatroomList = new ArrayList<Chatroom>();
        ArrayList<Chatroom.Message> messagesList = new ArrayList<Chatroom.Message>();
        messagesList.add(new Chatroom.Message("salam", "maryam"));
        messagesList.add(new Message("hi", "golnar"));
        Chatroom c1 = new Chatroom("maryam","salam goli","27/3/99","5",R.drawable.person,messagesList);
        Chatroom c2 = new Chatroom("golnar","salaam","23/4/99","4",R.drawable.person2, messagesList);
        chatroomList.add(c1);
        chatroomList.add(c2);
        recycler = getView().findViewById(R.id.rv_list);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        ListAdapter adapter = new ListAdapter(getActivity(), chatroomList);
        recycler.setAdapter(adapter);

    }


    class ListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        public ListAdapter(FragmentActivity context, ArrayList<Chatroom> items) {
            this.items = items;
        }

        private Context context;
        protected ArrayList<Chatroom> items;

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ListViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_list_chatroom, parent, false));

        }


        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {

            final Chatroom element = items.get(position);

            final ListViewHolder holder = (ListViewHolder) viewHolder;

            holder.text.setText(element.getTitle());
            holder.textView2.setText(element.getLastMessage());
            holder.textView3.setText(element.getDate());
            holder.textView4.setText(element.getMessageCoundt());
            holder.imageView.setImageResource(element.getAvatar());
            holder.parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    {
                        Intent i = new Intent(getActivity(), ChatroomActivity.class);
                        i.putExtra("chatroom",  element.getMessageList());

                        startActivity(i);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }


    }

    static class ListViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout parent;
        TextView text;
        ImageView imageView;
        TextView textView2;
        TextView textView3;
        TextView textView4;

        ListViewHolder(@NonNull View itemView) {
            super(itemView);
            parent = itemView.findViewById(R.id.cl_item);
            text = itemView.findViewById(R.id.tv_title);
            textView2 = itemView.findViewById(R.id.tv_desc);
            textView3 = itemView.findViewById(R.id.tv_time);
            textView4 = itemView.findViewById(R.id.tv_unread);
            imageView = itemView.findViewById(R.id.iv_icon);
        }
    }
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                Chatroom chatroom = (Chatroom) data.getSerializableExtra("new_task");
                chatroomList.add(chatroom);
                ((ListAdapter) recycler.getAdapter()).notifyDataSetChanged();
            }
        }
    }


}



