package io.golnar.plato.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import io.golnar.plato.R;
import io.golnar.plato.models.Chatroom;

public class ChatroomActivity extends AppCompatActivity {

    public ArrayList<Chatroom.Message> sentMessageList;
    private RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatroom);
        sentMessageList = ((ArrayList<Chatroom.Message>) getIntent().getSerializableExtra("chatroom"));
        init();
    }

    private void init() {

        initEvents();
//        messageList=new ArrayList< Chatroom.Message >();
//        messageList.add(new Chatroom.Message("dsf","asfc"));
//        messageList.add(new Chatroom.Message("helloo","yo"));
//        messageList.add(new Chatroom.Message("dsf","asfc"));
        recycler = findViewById(R.id.rv_list_chat);
        recycler.setLayoutManager(new LinearLayoutManager(this));
        ListAdapter adapter = new ListAdapter(this, sentMessageList);
        recycler.setAdapter(adapter);



    }




            private void initEvents() {

                View back = (View) findViewById(R.id.back_button_create_group_activity);
                back.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        onBackPressed();
                    }
                });
                View view = (View) findViewById(R.id.sent_button);
                view.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        EditText editText = (EditText) findViewById(R.id.et_chat);
                        String content = editText.getText().toString(); //gets you the contents of edit text
                        sentMessageList.add(new Chatroom.Message(content,""));
                        init();
                        editText.getText().clear();
                    }
                });
    }

    class ListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        public ListAdapter(Context context, ArrayList<Chatroom.Message> items) {
            this.items = items;
        }

        private Context context;
        protected ArrayList<Chatroom.Message> items;

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_message, parent, false));

        }


        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {

            final Chatroom.Message element = items.get(position);

            final ListViewHolder holder = (ListViewHolder) viewHolder;

            holder.message.setText(element.getMessage());
            holder.sender.setText(element.getSender());

        }

        @Override
        public int getItemCount() {
            return items.size();
        }


    }

    static class ListViewHolder extends RecyclerView.ViewHolder {

        ConstraintLayout parent;
        TextView message;
        TextView sender;

        ListViewHolder(@NonNull View itemView) {
            super(itemView);
            parent = itemView.findViewById(R.id.cl_message);
            message = itemView.findViewById(R.id.tv_message);
            sender = itemView.findViewById(R.id.tv_sender);
        }
    }
}
