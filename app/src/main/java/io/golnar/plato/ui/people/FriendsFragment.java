package io.golnar.plato.ui.people;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import io.golnar.plato.R;

public class FriendsFragment extends Fragment implements AddFriendDialog.OnInputSelected {
    private static final String TAG = "FriendsFragment";

    @Override
    public void sendInput(String platoID) {
        Log.i(TAG, "sendInput: found incoming input: " + platoID);
        Toast.makeText(getContext(), "Add Friend", Toast.LENGTH_SHORT).show();
        usernameList.add(platoID);
    }

    private FloatingActionButton addFab;
    //this part will be changed
    List<String> usernameList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.people_fragment_friends, container, false);

        addFab = root.findViewById(R.id.add_fab_friends_fragment);
        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: opening dialog");

                AddFriendDialog dialog = new AddFriendDialog();
                dialog.setTargetFragment(FriendsFragment.this, 1);
                dialog.show(getFragmentManager(), "AddFriendDialog");
            }
        });

        /*
                get list of friends
         */
        usernameList.add("Golnar");
        usernameList.add("Maryam");

        RecyclerView recyclerView = root.findViewById(R.id.friends_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        FriendsListAdapter adapter = new FriendsListAdapter(usernameList, getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        Log.i(TAG, "onCreateView: list is shown.");

        return root;
    }
}
