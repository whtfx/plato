package io.golnar.plato.ui.people;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import io.golnar.plato.R;

public class GroupDescriptionDialog extends AppCompatDialogFragment {

    private String currentDescription;
    private EditText editTextDescription;
    GroupDescriptionDialogListener listener;

    public GroupDescriptionDialog(String currentDescription) {
        this.currentDescription = currentDescription;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_custom_dialog, null);

        builder.setView(view)
                .setTitle("Choose Description")
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String des = editTextDescription.getText().toString();
                listener.applyTextDescription(des);
            }
        });

        editTextDescription = view.findViewById(R.id.input_edit_text_dialog);

        if (!currentDescription.equals("")) {
            editTextDescription.setText(currentDescription);
        }
        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (GroupDescriptionDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement GroupDescriptionDialogListener");
        }
    }

    public interface GroupDescriptionDialogListener {
        void applyTextDescription(String description);
    }
}



