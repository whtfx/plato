package io.golnar.plato.ui.people;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.golnar.plato.R;

public class FriendsListAdapter extends RecyclerView.Adapter {

    private static final String TAG = "FriendsListAdapter";

    ///this part will be changed:
    List<String> friendsUsername;
//    List<ImageView> friendsImage;

    Context mContext;

    public FriendsListAdapter(List<String> friendsUsername, Context mContext) {
        this.friendsUsername = friendsUsername;
        this.mContext = mContext;
//        this.friendsImage = friendsImage;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friend, parent, false);
        return new FriendViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Log.i(TAG, "onBindViewHolder: called");
        FriendViewHolder fvh = (FriendViewHolder) holder;
        fvh.bind(friendsUsername.get(position), mContext);
    }

    @Override
    public int getItemCount() {
        return friendsUsername.size();
    }

    private class FriendViewHolder extends RecyclerView.ViewHolder {

        //        private ImageView imageView;
        private TextView usernameText;
        private LinearLayout parentLayout;

        public FriendViewHolder(@NonNull View itemView) {
            super(itemView);
//            imageView = itemView.findViewById(R.id.friend_image);
            usernameText = itemView.findViewById(R.id.friend_username);
            parentLayout = itemView.findViewById(R.id.friend_parent_layout);
        }

        public void bind(String username, final Context context) {
            usernameText.setText(username);
            parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "Chat page will be created.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
