package io.golnar.plato.ui.games.xox;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

import io.golnar.plato.R;
import io.golnar.plato.ui.games.CasualFragment;
import io.golnar.plato.ui.games.GameGuessWordsActivity;
import io.golnar.plato.ui.games.LeaderBoardFragment;
import io.golnar.plato.ui.games.RankedFragment;
import io.golnar.plato.ui.games.ViewPagerAdapter;
import io.golnar.plato.ui.games.dotsAndBox.StartDotsActivity;

public class PrepareGameActivity extends AppCompatActivity {

    private static final String TAG = "XOXActivity";

    private ImageView backButton;

    private TabLayout tabLayout;
    private AppBarLayout appBarLayout;
    private ViewPager viewPager;

    private ProgressBar progressBarAnimation;
    private ObjectAnimator progressAnimator;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prepare_game_activity);

        Intent intent = this.getIntent();

        /* Obtain String from Intent  */
        ImageView imageView = findViewById(R.id.iv_game_photo);
        TextView textView = findViewById(R.id.tv_game_name);
        Button playButton = findViewById(R.id.play_button_casual);

        if (intent != null) {
            String strdata = intent.getExtras().getString("Guess");
            if (strdata.equals("From_Grid_B")) {
                playButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent playingIntent = new Intent(PrepareGameActivity.this, GameGuessWordsActivity.class);
                        startActivity(playingIntent);
                    }
                });
                textView.setText("GUESS WORDS");
                imageView.setImageDrawable(getDrawable(R.drawable.guess_word));
            } else if (strdata.equals("From_Grid_A")) {
                imageView.setImageDrawable(getDrawable(R.drawable.xox));
                textView.setText("XOX");
                playButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent playingIntent = new Intent(PrepareGameActivity.this, XoxActivity.class);
                        startActivity(playingIntent);
                    }
                });
            }else if (strdata.equals("From_Grid_C")) {
                imageView.setImageDrawable(getDrawable(R.drawable.img_dots));
                textView.setText("Dots");
                playButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent playingIntent = new Intent(PrepareGameActivity.this, StartDotsActivity.class);
                        startActivity(playingIntent);
                    }
                });
            }

        }


        Log.i(TAG, "onCreate: xox page is created.");

        tabLayout = (TabLayout) findViewById(R.id.tab_layout_xox_game);
        appBarLayout = (AppBarLayout) findViewById(R.id.appbar_xox_activity);
        viewPager = findViewById(R.id.view_pager_xox_activity);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        //add fragments to tab bar
        adapter.addFragment(new CasualFragment(), "Casual");
        adapter.addFragment(new RankedFragment(), "Ranked");
        adapter.addFragment(new LeaderBoardFragment(), "LeaderBoard");
        //adapter setup
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        //progress bar
        progressBarAnimation = findViewById(R.id.progressBar_xox);
        progressAnimator = ObjectAnimator.ofInt(progressBarAnimation, "progress", 0, 5);
        /*
        progressAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {

                if(user.winXOX == 5) {
                    progressBarAnimation.clearAnimation();
                    level++;
                }

            }
        });

         */

        ///////

        backButton = findViewById(R.id.back_icon_xox_activity);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

}