package io.golnar.plato.ui.games;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

import io.golnar.plato.R;
import io.golnar.plato.ui.games.xox.PrepareGameActivity;

public class GameGuessWordsActivity extends AppCompatActivity {

    private LinearLayout llGuessGame;
    private View item;
    private TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_guess_words);
        initGame();
        init();
    }

    String[] secret_word;
    char[] secret;
    char[] blanks;
    private int index = 0;

    Random r;
    int x;
    int correctTimes = 0;
    String secWord;
    String dash;
    String help;
    int cnt;
    boolean flag;

    String win;

    private void init() {
    }

    private void initGame() {
        secret_word = new String[4];
        secret_word[0] = "setar";
        secret_word[1] = "domino";
        secret_word[2] = "livan";
        secret_word[3] = "neyrang";
        r = new Random();
        x = r.nextInt(secret_word.length);
        secWord = secret_word[0];

        for (int i = 0; i < secWord.length(); i++) {
            llGuessGame = findViewById(R.id.ll_guess_game);
            item = LayoutInflater.from(this).inflate(R.layout.item_list_guess_game_words, llGuessGame, false);
            textView = item.findViewById(R.id.tv_item_word);

            llGuessGame.addView(item);

        }

        Button buttonSendWord = findViewById(R.id.send_word);

        final char[] stringToCharArray = secWord.toCharArray();

        final EditText getWord = findViewById(R.id.et_get_word);

        final TextView inCorrect = findViewById(R.id.tv_correct_or_not);


        buttonSendWord.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                if (index == secWord.length() - 1) {
                    finishF();
                } else {
                    char word = getWord.getText().toString().charAt(0);
                    if (stringToCharArray[index] == word) {
                        TextView items = (TextView) llGuessGame.getChildAt(index);
                        items.setText(String.valueOf(word));
                        inCorrect.setText("Correct guess");
                        getWord.getText().clear();
                        correctTimes = correctTimes + 1;
                    } else {
                        inCorrect.setText("Incorrect guess");
                        getWord.getText().clear();
                    }
                    index++;


                }
            }
        });


    }

    private void win() {
        String message = "You win.\n";
        showAlertDialog(message);
    }

    private void gameOver() {
        String message = "Game over.\n";
        showAlertDialog(message);
    }

    private void showAlertDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("End Game");
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent();
                intent.setClass(GameGuessWordsActivity.this, PrepareGameActivity.class);
                intent.putExtra("Guess","From_Grid_B");
                startActivity(intent);
            }
        });
        final AlertDialog alert = builder.create();
        alert.show();

    }

    public void finishF() {
        if ((correctTimes == secWord.length()-1)) {
            win();
        } else {
            gameOver();
        }
    }
}
