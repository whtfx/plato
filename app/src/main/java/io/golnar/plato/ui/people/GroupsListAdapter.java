package io.golnar.plato.ui.people;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import io.golnar.plato.R;

public class GroupsListAdapter extends RecyclerView.Adapter {

    private static final String TAG = "GroupsListAdapter";

    ///this part will be changed:
    List<String> groupNamesList;
    List<String> descriptionList;
//    List<ImageView> groupsImage;

    Context mContext;

    public GroupsListAdapter(List<String> groupNamesList, List<String> descriptionList, Context mContext) {
        this.groupNamesList = groupNamesList;
        this.descriptionList = descriptionList;
        this.mContext = mContext;
//        this.groupsImage = groupsImage;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_group, parent, false);
        return new GroupViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Log.i(TAG, "onBindViewHolder: called");
        GroupViewHolder fvh = (GroupViewHolder) holder;
        fvh.bind(groupNamesList.get(position), descriptionList.get(position), mContext);
    }

    @Override
    public int getItemCount() {
        return groupNamesList.size();
    }

    private class GroupViewHolder extends RecyclerView.ViewHolder {

        //        private ImageView imageView;
        private TextView nameText;
        private TextView descriptionText;
        private LinearLayout parentLayout;

        public GroupViewHolder(@NonNull View itemView) {
            super(itemView);
//            imageView = itemView.findViewById(R.id.group_image);
            nameText = itemView.findViewById(R.id.group_name);
            descriptionText = itemView.findViewById(R.id.group_description);
            parentLayout = itemView.findViewById(R.id.group_parent_layout);
        }

        public void bind(String name, String description, final Context context) {
            nameText.setText(name);
            descriptionText.setText(description);
            parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, "Group page will be created.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
