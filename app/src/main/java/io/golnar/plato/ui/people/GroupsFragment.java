package io.golnar.plato.ui.people;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import io.golnar.plato.R;

public class GroupsFragment extends Fragment {
    private static final String TAG = "GroupsFragment";

    private FloatingActionButton addFab;

    //this part will be changed
    List<String> groupNameList = new ArrayList<>();
    List<String> descriptionList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.people_fragment_groups, container, false);
        addFab = root.findViewById(R.id.add_fab_groups_fragment);
        addFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "onClick: create new group");
                Intent createGroupIntent = new Intent(getActivity(), CreateGroupActivity.class);
                startActivity(createGroupIntent);
            }
        });



        /*
               list of groups
         */
        groupNameList.add("Test1");
        descriptionList.add("Description1");

        RecyclerView recyclerView = root.findViewById(R.id.group_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        GroupsListAdapter adapter = new GroupsListAdapter(groupNameList, descriptionList, getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        Log.i(TAG, "onCreateView: list is shown.");

        return root;
    }
}
