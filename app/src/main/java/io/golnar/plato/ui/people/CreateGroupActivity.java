package io.golnar.plato.ui.people;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import io.golnar.plato.R;

public class CreateGroupActivity extends AppCompatActivity implements GroupNameDialog.GroupNameDialogListener
        , GroupDescriptionDialog.GroupDescriptionDialogListener {

    private static final String TAG = "CreateGroupActivity";

    private List<String> usernameList = new ArrayList<>();

    private LinearLayout groupName;
    private LinearLayout groupDescription;
    private TextView nameText;
    private TextView descriptionText;

    private TextView createText;
    private ImageView groupImage;
    private Button backButton;

    Uri imageUri;

    private final int PICK_IMAGE = 100;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        Log.i(TAG, "onCreate: CreateGroup page is created.");

        groupName = (LinearLayout) findViewById(R.id.name_container_create_group);
        groupDescription = (LinearLayout) findViewById(R.id.description_container_create_group);
        nameText = (TextView) findViewById(R.id.name_text_group);
        descriptionText = (TextView) findViewById(R.id.description_text_group);

        backButton = findViewById(R.id.back_button_create_group_activity);
        groupImage = findViewById(R.id.image_group_create);
        createText = findViewById(R.id.create_group_text);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: back button is clicked.");
                onBackPressed();
            }
        });

//        FragmentManager manager = getSupportFragmentManager();
//        final FragmentTransaction transaction = manager.beginTransaction();

        createText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nameText.getText().toString().length() != 0 && descriptionText.getText().toString().length() != 0) {
                    //this part will be change
//                    Bundle bundle = new Bundle();
//                    bundle.putString("groupName", nameText.getText().toString());
//                    bundle.putString("groupDescription", descriptionText.getText().toString());
//                    GroupsFragment fragment = new GroupsFragment();
//                    fragment.setArguments(bundle);
//                    transaction.replace(R.id.group_parent_layout, fragment);
//                    transaction.commit();
                } else {
                    Toast.makeText(CreateGroupActivity.this, "Please enter name and description of group.", Toast.LENGTH_SHORT).show();
                }
            }});

        groupImage.setOnClickListener(new View.OnClickListener()

            {
                @Override
                public void onClick (View view){
                Log.i(TAG, "onClick: open gallery");
                openGallery();
            }
            });

        groupName.setOnClickListener(new View.OnClickListener()

            {
                @Override
                public void onClick (View view){
                Log.i(TAG, "onClick: open get name dialog");
                openNameDialog();
            }
            });

        groupDescription.setOnClickListener(new View.OnClickListener()

            {
                @Override
                public void onClick (View view){
                Log.i(TAG, "onClick: open dialog");
                openDescriptionDialog();
            }
            });

        


        /*
                get list of friends
         */
        usernameList.add("Golnar");
        usernameList.add("Maryam");

            RecyclerView recyclerView = findViewById(R.id.add_people_to_group_list);
        recyclerView.setLayoutManager(new

            LinearLayoutManager(CreateGroupActivity .this));
            AddPeopleToGroupAdapter adapter = new AddPeopleToGroupAdapter(usernameList);
        recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new

            DefaultItemAnimator());
        Log.i(TAG,"onCreateView: list is shown.");
        }

        public void openGallery () {
            Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            startActivityForResult(gallery, PICK_IMAGE);
        }

        @Override
        protected void onActivityResult ( int requestCode, int resultCode, Intent data){
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
                imageUri = data.getData();
                groupImage.setImageURI(imageUri);
            }
        }

        public void openNameDialog () {
            Log.i(TAG, "openNameDialog: open dialog");
            GroupNameDialog nameDialog = new GroupNameDialog(nameText.getText().toString());
            nameDialog.show(getSupportFragmentManager(), "name dialog");
        }

        @Override
        public void applyTextName (String name){
            if (!name.equals("")) {
                nameText.setText(name);
            }
        }

        public void openDescriptionDialog () {
            Log.i(TAG, "openDescriptionDialog: open dialog");
            GroupDescriptionDialog descriptionDialog = new GroupDescriptionDialog(descriptionText.getText().toString());
            descriptionDialog.show(getSupportFragmentManager(), "description dialog");
        }

        @Override
        public void applyTextDescription (String description){
            if (!description.equals("")) {
                descriptionText.setText(description);
            }
        }
    }
