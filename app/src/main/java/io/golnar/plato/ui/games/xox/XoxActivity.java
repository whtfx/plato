package io.golnar.plato.ui.games.xox;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import io.golnar.plato.R;

public class XoxActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "XOXPlaying";

    private final int SIZE = 3;
    private boolean player1Turn;
    private int roundCount;

    private Button[][] buttons = new Button[SIZE][SIZE];
//    private TextView textViewPlayer1;
//    private TextView textViewPlayer2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xox);
        Log.i(TAG, "onCreate: xox is started");

//        textViewPlayer1 = findViewById(R.id.first_player_text_xox);
//        textViewPlayer2 = findViewById(R.id.second_player_text_xox);

        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                String buttonID = "button_" + i + j;
                int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                buttons[i][j] = findViewById(resID);
                buttons[i][j].setOnClickListener(this);
            }
        }
    }

    private boolean checkForWin() {
        String[][] fields = new String[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                fields[i][j] = buttons[i][j].getText().toString();
            }
        }
        for (int i = 0; i < SIZE; i++) {
            if (fields[i][0].equals(fields[i][1]) && fields[i][0].equals(fields[i][2])
                    && !fields[i][0].equals("")) {
                return true;
            }
        }
        for (int i = 0; i < SIZE; i++) {
            if (fields[0][i].equals(fields[1][i]) && fields[0][i].equals(fields[2][i])
                    && !fields[0][i].equals("")) {
                return true;
            }
        }
        if (fields[0][0].equals(fields[1][1]) && fields[0][0].equals(fields[2][2])
                && !fields[0][0].equals("")) {
            return true;
        }
        if (fields[0][2].equals(fields[1][1]) && fields[0][2].equals(fields[2][0])
                && !fields[0][2].equals("")) {
            return true;
        }
        return false;
    }

    private void player1Wins() {
        String message = "Player1 wins the game.\n";
        showAlertDialog(message);
    }

    private void player2Wins() {
        String message = "Player2 wins the game.\n";
        showAlertDialog(message);
    }

    private void draw() {
        String draw = "Draw!";
        showAlertDialog(draw);
    }

    private void showAlertDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("End Game");
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent();
                intent.setClass(XoxActivity.this, PrepareGameActivity.class);
                intent.putExtra("Guess","From_Grid_A");
                startActivity(intent);
            }
        });
        final AlertDialog alert = builder.create();
        alert.show();

    }

    @Override
    public void onClick(View view) {
        Log.i(TAG, "onClick: one of the cells is clicked");
        Button button = (Button) view;
        if (!button.getText().toString().equals("")) {
            return;
        }
        if (player1Turn) {
            button.setText("X");
            button.setTextColor(Color.parseColor("#FF002A"));
        } else {
            button.setText("O");
            button.setTextColor(Color.parseColor("#000000"));
        }
        roundCount++;
        if (checkForWin()) {
            if (player1Turn) {
                player1Wins();
            } else {
                player2Wins();
            }
        } else if (roundCount == SIZE * SIZE) {
            draw();
        } else {
            player1Turn = !player1Turn;
        }

    }
}

