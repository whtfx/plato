package io.golnar.plato.ui.people;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import io.golnar.plato.R;

public class AddPeopleToGroupAdapter extends RecyclerView.Adapter {

    private static final String TAG = "AddPeopleToGroupAdapter";

    ///this part will be changed:
    List<String> peopleUsername;
    List<Boolean> isSelected;
//    List<ImageView> friendsImage;

    public AddPeopleToGroupAdapter(List<String> peopleUsername) {
        this.peopleUsername = peopleUsername;
        isSelected = new ArrayList<>();
        for(int i = 0; i <peopleUsername.size(); i++){
            isSelected.add(false);
        }
//        this.friendsImage = friendsImage;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friend, parent, false);
        return new PersonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Log.i(TAG, "onBindViewHolder: called");
        PersonViewHolder pvh = (PersonViewHolder) holder;
        pvh.bind(peopleUsername.get(position), position);
    }

    @Override
    public int getItemCount() {
        return peopleUsername.size();
    }

    private class PersonViewHolder extends RecyclerView.ViewHolder {

        //        private ImageView imageView;
        private TextView usernameText;
        private LinearLayout parentLayout;

        public PersonViewHolder(@NonNull View itemView) {
            super(itemView);
//            imageView = itemView.findViewById(R.id.friend_image);
            usernameText = itemView.findViewById(R.id.friend_username);
            parentLayout = itemView.findViewById(R.id.friend_parent_layout);
        }

        public void bind(String username, final int position) {
            usernameText.setText(username);
            parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(isSelected.get(position)){
                        parentLayout.setBackgroundColor(Color.parseColor("#ffffff"));
                        isSelected.set(position, false);
                    }else{
                        parentLayout.setBackgroundColor(Color.parseColor("#BCF696"));
                        isSelected.set(position, true);
                    }
                }
            });
        }
    }
}

