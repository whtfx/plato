package io.golnar.plato.ui.games;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import io.golnar.plato.ProfileActivity;
import io.golnar.plato.R;
import io.golnar.plato.ui.games.xox.PrepareGameActivity;

public class GamesFragment extends Fragment {

    private static final String TAG = "GamesFragment";

    ImageView profile, xoxImage,guessImage,dotsImage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: game fragment is created.");

        View root = inflater.inflate(R.layout.fragment_games, container, false);
        profile = root.findViewById(R.id.profile_toolbar_games);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent profileIntent = new Intent(getActivity(), ProfileActivity.class);
                startActivity(profileIntent);
            }
        });

        dotsImage = root.findViewById(R.id.iv_dots);
        dotsImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: dots game is selected.");
                Intent intent = new Intent();
                intent.setClass(getActivity(), PrepareGameActivity.class);
                intent.putExtra("Guess","From_Grid_C");
                startActivity(intent);
            }
        });

        xoxImage = root.findViewById(R.id.xox_image_view_games_fragment);
        xoxImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: Xox game is selected.");
                Intent intent = new Intent();
                intent.setClass(getActivity(), PrepareGameActivity.class);
                intent.putExtra("Guess","From_Grid_A");
                startActivity(intent);
            }
        });

        guessImage = root.findViewById(R.id.iv_guss_words);
        guessImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i(TAG, "onClick: Guess game is selected.");
                Intent intent = new Intent();
                intent.setClass(getActivity(), PrepareGameActivity.class);
                intent.putExtra("Guess","From_Grid_B");
                startActivity(intent);
            }
        });

        return root;
    }
}
