package io.golnar.plato;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

public class UsernameDialog extends AppCompatDialogFragment {
    private String currentUsername;
    private EditText editTextUsername;
    private UsernameDialogListener listener;

    public UsernameDialog(String currentUsername) {
        this.currentUsername = currentUsername;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_custom_dialog, null);

        builder.setView(view)
                .setTitle("Change Plato ID")
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String username = editTextUsername.getText().toString();
                listener.applyTextUsername(username);
            }
        });

        editTextUsername = view.findViewById(R.id.input_edit_text_dialog);

        if (currentUsername.equals("Say something here...")) {
            editTextUsername.setHint("Say something here...");
        } else {
            editTextUsername.setText(currentUsername);
        }

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (UsernameDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement UsernameDialogListener");
        }
    }

    public interface UsernameDialogListener {
        void applyTextUsername(String username);
    }
}


