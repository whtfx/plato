package io.golnar.plato;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.DataInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;
import java.net.Socket;

public class LoginActivity extends AppCompatActivity {
    EditText usernameEditText, passwordEditText;
    Button loginButton;
    String username, password;

    private static final String TAG = "LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Log.i(TAG, "onCreate: login page is created.");
        usernameEditText = findViewById(R.id.username_login);
        passwordEditText = findViewById(R.id.password_login);
        loginButton = findViewById(R.id.button_login_activity);

        // Check Username is Empty or NOT
        usernameEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (passwordEditText.length() == 0)
                    usernameEditText.setError("Please enter username.");
            }
        });

        // Check Password is Empty or NOT
        passwordEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (passwordEditText.length() == 0)
                    passwordEditText.setError("Please enter password.");
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: login button is clicked.");
                username = usernameEditText.getText().toString();
                password = passwordEditText.getText().toString();
                if (username.isEmpty() || password.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "Please enter username and password", Toast.LENGTH_LONG).show();
                } else {
                    LoginCheck sendLogin = new LoginCheck(LoginActivity.this);
                    sendLogin.execute("Login", username, password);
                }
            }
        });
    }
}

// Class For BackGround Socket Login
class LoginCheck extends AsyncTask<String, Void, String> {
    Socket socket;
    ObjectOutputStream outputStream;
    ObjectInputStream inputStream;
    Boolean answer;
    WeakReference<LoginActivity> activityReference;
    User user;

    LoginCheck(LoginActivity context) {
        activityReference = new WeakReference<>(context);
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            socket = new Socket(activityReference.get().getResources().getString(R.string.ip), 8080);
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            inputStream = new ObjectInputStream(socket.getInputStream());
            outputStream.writeObject(strings);
            outputStream.flush();
            answer = inputStream.readBoolean();
            if (answer) {
                user = (User) inputStream.readObject();
            }
            outputStream.close();
            inputStream.close();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        LoginActivity activity = activityReference.get();
        if (activity == null || activity.isFinishing()) return;

        if (answer) {
            Toast.makeText(activity, "You logged in successfully!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(activity, HomeActivity.class);
            intent.putExtra("user", user);
            activity.startActivity(intent);
        } else {}

    }

}