package io.golnar.plato;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ref.WeakReference;
import java.net.Socket;

public class RegisterActivity extends AppCompatActivity {
    ImageButton profileImage;
    EditText usernameEdithText, passwordEdithText;
    Button registerButton;
    Uri imageUri;
    private final int PICK_IMAGE = 100;
    boolean isWrongPassword = false;
    boolean isWrongUsername = false;
    boolean usernameLength = false;
    byte[] imageBytes;

    private static final String TAG = "RegisterActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Log.i(TAG, "onCreate: register page is created.");
        profileImage = (ImageButton) findViewById(R.id.user_picture_register);
        usernameEdithText = (EditText) findViewById(R.id.username_register);
        passwordEdithText = (EditText) findViewById(R.id.password_register);
        registerButton = findViewById(R.id.button_register_activity);

        if (passwordEdithText.length() == 0) {
            passwordEdithText.setError("Please enter password.");
            isWrongPassword = true;
        } else if (passwordEdithText.length() < 6) {
            passwordEdithText.setError("Password must have more than 5 characters.");
            isWrongPassword = true;
        } else {
            passwordEdithText.setError(null);
            isWrongPassword = false;
        }

        if (usernameEdithText.length() == 0) {
            usernameEdithText.setError("Please enter your username.");
            usernameLength = true;
        }

        //check username
        usernameEdithText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (usernameEdithText.length() == 0) {
                    usernameEdithText.setError("Please enter your username.");
                    usernameLength = true;
                } else
                    usernameEdithText.setError(null);
                usernameLength = false;///////////////////////////////////////////////////////////////////////////

                new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        try {
                            Socket socket = new Socket(getResources().getString(R.string.ip), 8080);
                            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());

                            String[] sendData = {"CheckUsername", usernameEdithText.getText().toString()};
                            outputStream.writeObject(sendData);
                            outputStream.flush();

                            if (!inputStream.readBoolean()) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        usernameEdithText.setError("This username is already user.");
                                        isWrongUsername = true;
                                    }
                                });
                            } else {
                                isWrongPassword = false;
                            }

                            outputStream.close();
                            inputStream.close();
                            socket.close();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (usernameEdithText.length() == 0) {
                    usernameEdithText.setError("Please enter username.");
                    usernameLength = true;
                } else
                    usernameEdithText.setError(null);
                usernameLength = false;

                new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        try {
                            Socket socket = new Socket(getResources().getString(R.string.ip), 8080);
                            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());

                            String[] sendData = {"CheckUsername", usernameEdithText.getText().toString()};
                            outputStream.writeObject(sendData);
                            outputStream.flush();

                            if (inputStream.readBoolean()) {
                                isWrongPassword = false;
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        usernameEdithText.setError("This username is already user.");
                                        isWrongUsername = true;
                                    }
                                });
                            }

                            outputStream.close();
                            inputStream.close();
                            socket.close();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //check password
        passwordEdithText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (passwordEdithText.length() == 0) {
                    passwordEdithText.setError("Please enter your password.");
                    isWrongPassword = true;
                } else if (passwordEdithText.length() < 6) {
                    passwordEdithText.setError("Password must have more than 5 characters.");
                    isWrongPassword = true;
                } else {
                    passwordEdithText.setError(null);
                    isWrongPassword = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (passwordEdithText.length() == 0) {
                    passwordEdithText.setError("Please enter your password.");
                    isWrongPassword = true;
                } else if (passwordEdithText.length() < 6) {
                    passwordEdithText.setError("Password must have more than 5 characters.");
                    isWrongPassword = true;
                } else {
                    passwordEdithText.setError(null);
                    isWrongPassword = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: picture button is clicked.");
                openGallery();
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "onClick: register button is clicked.");
                if (!isWrongPassword && !isWrongPassword && !usernameLength) {
                    String username = usernameEdithText.getText().toString();
                    String password = passwordEdithText.getText().toString();
                    SendRegister sendRegister = new SendRegister(RegisterActivity.this);
                    if (profileImage.getDrawable() !=null) {
                        Bitmap bmp = ((BitmapDrawable) profileImage.getDrawable()).getBitmap();
                        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                        bmp.compress(Bitmap.CompressFormat.JPEG, 1, outputStream);
                        imageBytes = outputStream.toByteArray();
                    }
                    else {
                        imageBytes = new byte[1];
                    }
                    sendRegister.execute(imageBytes,"Register", username, password);
                }
                else {
                    Toast.makeText(RegisterActivity.this, "Please enter correct username and password.", Toast.LENGTH_LONG).show();
                    Log.i(TAG, "onClick: change page");
                }
            }
        });
    }

    public void openGallery() {
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery, PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imageUri = data.getData();
            profileImage.setImageURI(imageUri);
        }else{}
    }
}

// Class For BackGround Socket Login
class SendRegister extends AsyncTask<Object, Void, String> {
    Socket socket;
    ObjectOutputStream outputStream;
    ObjectInputStream inputStream;
    DataInputStream dataInputStream;
    Boolean answer;
    User user;
    WeakReference<RegisterActivity> activityReference;

    SendRegister(RegisterActivity context) {
        activityReference = new WeakReference<>(context);
    }

    @Override
    protected String doInBackground(Object... input) {
        try {
            socket = new Socket(activityReference.get().getResources().getString(R.string.ip), 8080);
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            inputStream = new ObjectInputStream(socket.getInputStream());
            String[] strings = {(String) input[1], (String) input[2], (String) input[3]};
            byte[] imgByte = (byte[]) input[0];
            outputStream.writeObject(strings);
            outputStream.flush();
            outputStream.writeObject(imgByte);
            outputStream.flush();
            answer = inputStream.readBoolean();
            if (answer) {
                user = (User) inputStream.readObject();
            }

            outputStream.close();
            inputStream.close();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        RegisterActivity activity = activityReference.get();
        if (activity == null || activity.isFinishing()) return;

        if (answer) {
            Toast.makeText(activity, "You registered successfully", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(activity, HomeActivity.class);
            intent.putExtra("user", user);
            activity.startActivity(intent);
        } else {
        }
    }
}
