package io.golnar.plato.models;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

import io.golnar.plato.R;

public class ClientActivity extends AppCompatActivity {
    private static final String TAG = "ClientActivity";//m

    EditText serverIp, smessage;
    TextView chat;
    Button sent;
    View connectPhones;
    String serverIpAddress = "10.0.2.2", msg = "", str;//m ->10.0.2.2
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatroom);
        chat = (TextView) findViewById(R.id.tv_alaki);
        serverIp = (EditText) findViewById(R.id.server_ip);
        smessage = (EditText) findViewById(R.id.et_chat);
        sent = (Button) findViewById(R.id.sent_button);
        Log.i(TAG, "onCreate: find all views");//m
        sent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread sentThread = new Thread(new sentMessage());
                sentThread.start();
                Log.i(TAG, "onClick: send button is clicked");//m
            }
        });
        connectPhones = (View) findViewById(R.id.connect_phones);
        connectPhones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serverIpAddress = serverIp.getText().toString();
                if (!serverIpAddress.equals("")) {
                    Thread clientThread = new Thread(new ClientThread());
                    clientThread.start();
                }
            }
        });
    }

    class sentMessage implements Runnable {
        @Override

        public void run() {

            try {
                InetAddress serverAddr =
                        InetAddress.getByName(serverIpAddress);
                Socket socket = new Socket(serverAddr, 52384); //
//                create client socket
                DataOutputStream os = new DataOutputStream(socket.getOutputStream());
                DataInputStream in = new DataInputStream(socket.getInputStream());
                str = smessage.getText().toString();
                Log.i(TAG, "run: message -> " + str);//m
//                str = str + "\n";
//                msg = msg + "Client : " + str;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
//                        chat.setText(msg);
                    }
                });

                os.writeUTF(str);
                os.flush();

                Thread.sleep(3000);
                Log.d("//", "run: ");

                os.close();
                socket.close();
            } catch (IOException | InterruptedException e) {
//                Log.d("//", e.getMessage().toString());
                e.printStackTrace();
            }
        }
    }

    public class ClientThread implements Runnable {
        public void run() {
            try {
                while (true) {
                    InetAddress serverAddr = InetAddress.getByName(serverIpAddress);
                    Socket socket = new Socket(serverAddr, 52384); //
//                    create client socket

                    DataInputStream in = new DataInputStream(socket.getInputStream());
                    String line = null;
                    while ((line = in.readLine()) != null) {
                        msg = msg + "Server : " + line + "\n";
//                        handler.post(new Runnable() {
//                            @Override
//                            public void run() {
//                                chat.setText(msg);
//                            }
//                        });
                    }
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),
                            true);
                    out.println("data");
                    out.flush();
                    in.close();
                    socket.close();
                    Thread.sleep(100);
                }
            } catch (Exception e) {
                e.printStackTrace();
//                Log.d("//", e.getMessage().toString());
            }
        }
    }
}