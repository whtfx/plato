package io.golnar.plato.models;

import android.graphics.drawable.Drawable;

import java.io.Serializable;
import java.util.ArrayList;

public class Chatroom implements Serializable {
    String title,lastMessage,date;
    String messageCoundt;
    int avatar;
    ArrayList<Message> messageList;

    public String getTitle() {
        return title;
    }

    public Chatroom(String title, String lastMessage, String date, String messageCoundt, int avatar, ArrayList<Message> messageList) {
        this.title = title;
        this.lastMessage = lastMessage;
        this.date = date;
        this.messageCoundt = messageCoundt;
        this.avatar = avatar;
        this.messageList = messageList;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }




    public String getMessageCoundt() {
        return messageCoundt;
    }

    public void setMessageCoundt(String messageCoundt) {
        this.messageCoundt = messageCoundt;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Message> getMessageList() {
        return messageList;
    }

    public void setMessageList(ArrayList<Message> messageList) {
        this.messageList = messageList;
    }



   public static class Message implements Serializable {
        String message;
        String sender;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getSender() {
            return sender;
        }

        public void setSender(String sender) {
            this.sender = sender;
        }

        public Message(String message, String sender) {
            this.message = message;
            this.sender = sender;
        }
    }
}

