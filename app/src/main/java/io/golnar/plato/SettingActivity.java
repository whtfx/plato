package io.golnar.plato;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class SettingActivity extends AppCompatActivity {
    private static final String TAG = "SettingActivity";

    Button backButton;
    TextView logoutText, aboutText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Log.i(TAG, "onCreate: setting page is created.");

        logoutText = findViewById(R.id.logout_text);
        aboutText = findViewById(R.id.about_us_text);
        backButton = findViewById(R.id.back_button_setting_activity);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        logoutText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG, "onClick: back to main page");
                Intent mainIntent = new Intent(SettingActivity.this, MainActivity.class);
                startActivity(mainIntent);
            }
        });

        aboutText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent aboutUsIntent = new Intent(SettingActivity.this, AboutUsActivity.class);
                startActivity(aboutUsIntent);
            }
        });
    }
}
